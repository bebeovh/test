﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace OrangeTests
{

    [TestFixture]   //atrybut
    public class UnitTest1
    {
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Url = "https://opensource-demo.orangehrmlive.com/";
            driver.FindElement(By.XPath("//input[@id = 'txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.XPath("//input[@id = 'txtPassword']")).SendKeys("admin123");
            driver.FindElement(By.XPath("//input[@id = 'btnLogin']")).Click();
        }

        // nowy komentarz
        [Test, Order(1)]
        public void Logowanie()
        {
            
            IWebElement welcomeAdmin = driver.FindElement(By.XPath("//a[@id='welcome']"));

            Assert.IsNotNull(welcomeAdmin);
        }

        [Test, Order(2)]
        public void DashboardClick()
        {
            driver.Url = "https://opensource-demo.orangehrmlive.com/";
          
            driver.FindElement(By.XPath("//a[@id='menu_dashboard_index']/b")).Click();
            driver.FindElement(By.XPath("//span[@class='quickLinkText' and contains(text(), 'Timesheets')]")).Click();


        }


       // [TearDown]
       // public void TearDown ()

       //     {
       //     driver.Close();
        //    }

        }
    }

